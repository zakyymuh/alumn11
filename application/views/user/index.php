<nav class="navbar white navbar-inverse">
    <div class="col-md-6 col-xs-6 navbar-header">
      <a href="#">ALUMN<strong>11</strong></a>
	</div>
</nav>
<div class="container-fluid">
 <div class="col-md-3">
  <div data-spy="affix" data-offset-top="70">
   <div class="left-name text-left">
    <img src="<?php echo base_url();?>assets/img/user/img-small.jpg" class="img img-rounded img-responsive pull-left">
    <p><b>Zaky Muhammad Yopi Rusyana</b><br><span class="small">online</span></p>
   </div>
   <div class="left-nav">
    <ul>
    <li class="h-nav"><strong>MENU</strong></li>
    <li><span class="fa fa-user"></span>  <a href="#">Profile</a></li>
    <li><span class="fa fa-lock"></span> <a href="#">Ganti Password</a></li>
    <li><span class="fa fa-calendar"></span> <a href="#">Event</a></li>
    <li><span class="fa fa-sign-out"></span> <a href="#">Keluar</a></li>
    </ul>
   </div>
  </div>
 </div>
 <div class="col-md-9">
 <div class="profile">
  <div class="top-profile">
  <div class="img-profile pull-left"> 
  <img src="<?php echo base_url();?>assets/img/user/img.jpg" class="img img-rounded img-responsive">
  </div>
  <div class="name-profile">
   <p class="main-name"><b>Zaky Muhammad Yopi Rusyana</b></span></p>
   <p class="text-muted">Lorem ipsum dolor sit amet, consetetur sadipscing elitr,  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
  </div>
  <div class="col-md-2"><strong>Participate</strong><br><span class="fa fa-check" aria-hidden="true"> 99</span>
  </div>
  <div class="col-md-2"><strong>Event</strong><br><span class="fa fa-calendar" aria-hidden="true"> 0</span>
  </div>
 </div>
 <hr>
 <div class="body-profile">
  <div class="row">
   <div class="col-md-6">
   <h3>Contact</h3>
   <p><span class="fa fa-phone" aria-hidden="true"> 081-313-331-2<p>
   <p><span class="fa fa-location-arrow" aria-hidden="true"> invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua, Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit. Labore<p>
   <p><span class="fa fa-envelope" aria-hidden="true"> Lorem@gadho.com<p>
   </div>
   <div class="col-md-6">
   <h3>Sosial Media</h3>
   <p><span class="fa fa-facebook-square" aria-hidden="true"> <a href='#'>ALUMN11</a><p>
   <p><span class="fa fa-twitter-square" aria-hidden="true"> <a href='#'>ALUMN11</a><p>
   <p><span class="fa fa-instagram" aria-hidden="true"> <a href='#'>ALUMN11</a><p>
   <p><span class="fa fa-snapchat-square" aria-hidden="true"> <a href='#'>ALUMN11</a><p>
   <p><span class="fa fa-pinterest-square" aria-hidden="true">  <a href='#'>ALUMN11</a><p>
   <p><span class="fa fa-google-plus-square" aria-hidden="true">  <a href='#'>ALUMN11</a><p>
   <p><span class="fa fa-steam-square" aria-hidden="true">  <a href='#'>ALUMN11</a><p>
   <p><span class="fa fa-github-square" aria-hidden="true">  <a href='#'>ALUMN11</a><p>
   <p><span class="fa fa-linkedin-square" aria-hidden="true">  <a href='#'>ALUMN11</a><p>
   <p><span class="fa fa-youtube" aria-hidden="true">  <a href='#'>ALUMN11</a><p>
   </div>
  </div>
 </div>
 </div>
 </div>
</div>